# RFI Map

This is a basic galactic map of the Requiem For Innocence universe. It's designed as a supplement to the wiki at 
https://rfiuniverse.com.

## Features

These are the current or planned features of the map:

* [ ] Admin
  * [ ] Add/Edit star systems
  * [ ] Add/Edit points of interest
  * [ ] Add/Edit trade routes
  * [ ] Add/Edit factions
  * [ ] Add/Edit subspace anomalies
  * [ ] Add/Edit faction boundaries
* [ ] Display
  * [X] Display map
  * [X] Display layers (generic)
    * [X] Support per-layer and per-feature styling
  * [X] Display faction boundaries
  * [X] Display points of interest
  * [ ] Display common trade routes
  * [ ] Display known subspace anomalies
  * [ ] Display star systems
    * [ ] Display star system names
    * [ ] Link to Wiki page for star system
* [ ] Search
  * [ ] Search for star systems
  * [ ] Search for points of interest
  * [ ] Search for trade routes
  * [ ] Search for factions
* [X] Measure
  * [X] Measure total distance of a drawn path
  * [X] Display total time of the trip, based on subspace speed

## Getting Started

To get started, run `npm install`, and then kick off the dev server with `npm run dev`. To build for production, 
run `npm run build`.

