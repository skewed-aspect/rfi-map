#-----------------------------------------------------------------------------------------------------------------------
# Bundle Stage - Do all our bundling of assets
#-----------------------------------------------------------------------------------------------------------------------

FROM node:20-alpine AS bundle-builder

RUN mkdir -p /app
WORKDIR /app

ADD . /app/

# better-sqlite3 build deps
RUN apk add build-base python3

RUN npm ci --no-fund
RUN npm run build

#-----------------------------------------------------------------------------------------------------------------------
# Dep Stage - Install production packages and clean cache
#-----------------------------------------------------------------------------------------------------------------------

FROM node:20-alpine AS npm-builder

COPY --from=bundle-builder /app /app

WORKDIR /app

# This gets around the `husky install` error
RUN npm pkg delete scripts.prepare && npm ci --no-fund --omit=dev --omit=optional

#-----------------------------------------------------------------------------------------------------------------------
# Final Docker
#-----------------------------------------------------------------------------------------------------------------------

FROM node:20-alpine
EXPOSE 4500

MAINTAINER Christopher S. Case <chris.case@g33xnexus.com>

# Only copy the files we actually need
COPY --from=bundle-builder /app/dist /app/dist
COPY --from=npm-builder /app/node_modules /app/node_modules
COPY --from=bundle-builder /app/package.json /app/

RUN mkdir /app/db

ADD ./config /app/config
ADD ./tiles /app/tiles

# Set the environment for the configuration
ENV ENVIRONMENT="local"

WORKDIR /app

VOLUME /app/db

CMD [ "node", "dist/server/server.js", "# rfi-map server" ]

#-----------------------------------------------------------------------------------------------------------------------
