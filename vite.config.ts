//----------------------------------------------------------------------------------------------------------------------

import 'dotenv/config';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import Components from 'unplugin-vue-components/vite';
import { BootstrapVueNextResolver } from 'unplugin-vue-components/resolvers';

// Interfaces
import { ServerConfig } from './src/common/interfaces/config';

// Utils
import configUtil from '@strata-js/util-config';

// ---------------------------------------------------------------------------------------------------------------------
// Configuration
// ---------------------------------------------------------------------------------------------------------------------

const env = (process.env.ENVIRONMENT ?? 'local').toLowerCase();
configUtil.load(`./config/${ env }.yml`);

const config = configUtil.get<ServerConfig>();

//----------------------------------------------------------------------------------------------------------------------

// https://vitejs.dev/config/
export default defineConfig({
    root: 'src/client',
    publicDir: 'assets',
    plugins: [
        vue(),
        Components({
            resolvers: [ BootstrapVueNextResolver() ]
        })
    ],
    css: {
        preprocessorOptions: {
            scss: {
                quietDeps: true
            }
        }
    },
    server: {
        host: config.http.host,
        port: config.http.port,
        proxy: {
            // Add additional routes here
            '/auth': `http://127.0.0.1:${ config.http.port - 1 }`,
            '/account': `http://127.0.0.1:${ config.http.port - 1 }`,
            '/tile': `http://127.0.0.1:${ config.http.port - 1 }`,
            '/layer': `http://127.0.0.1:${ config.http.port - 1 }`,
            '/version': `http://127.0.0.1:${ config.http.port - 1 }`,
            '/socket.io': {
                target: `http://127.0.0.1:${ config.http.port - 1 }`,
                ws: true
            }
        }
    },
    build: {
        outDir: '../../dist/client',
        emptyOutDir: true,
        cssCodeSplit: true
    }
});

//----------------------------------------------------------------------------------------------------------------------
