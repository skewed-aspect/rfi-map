// ---------------------------------------------------------------------------------------------------------------------
// Account Resource Access
// ---------------------------------------------------------------------------------------------------------------------

// Models
import { Account, AccountSettings } from '../../common/models/account.js';

// Resource Access
import adminRA from './admin.js';

// Utils
import { getDB } from '../utils/database.js';
import { NotFoundError } from '../errors.js';

// ---------------------------------------------------------------------------------------------------------------------

/* eslint-disable camelcase */

class AccountResourceAccess
{
    private _toDB(account : Account) : Record<string, unknown>
    {
        return {
            account_id: account.id,
            email: account.email,
            name: account.name ?? null,
            avatar: account.avatar ?? null,
            settings: JSON.stringify(account.settings),
        };
    }

    private _fromDB(record : Record<string, unknown>) : Account
    {
        return {
            id: record.account_id as string,
            email: record.email as string,
            name: record.name as string ?? undefined,
            avatar: record.avatar as string ?? undefined,
            settings: JSON.parse(record.settings as string),
            isAdmin: adminRA.isAdmin(record.email as string),
        };
    }

    public async get(accountID : string) : Promise<Account>
    {
        const db = await getDB();
        const record = await db('account')
            .where({ account_id: accountID })
            .first();

        if(!record)
        {
            throw new NotFoundError(`Account with id "${ accountID }" not found.`);
        }

        return this._fromDB(record);
    }

    public async getByEmail(email : string) : Promise<Account>
    {
        const db = await getDB();
        const record = await db('account')
            .where({ email })
            .first();

        if(!record)
        {
            throw new NotFoundError(`Account with email "${ email }" not found.`);
        }

        return this._fromDB(record);
    }

    public async add(account : Account) : Promise<void>
    {
        const db = await getDB();
        await db('account')
            .insert(this._toDB(account));
    }

    public async update(account : Account) : Promise<void>
    {
        const db = await getDB();
        await db('account')
            .where({ account_id: account.id })
            .update(this._toDB(account))
            .update('updated', db.fn.now());
    }

    public async updateSettings(accountID : string, settings : AccountSettings) : Promise<void>
    {
        const db = await getDB();
        await db('account')
            .where({ account_id: accountID })
            .update({ settings: JSON.stringify(settings) });
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new AccountResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------
