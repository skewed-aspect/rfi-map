//----------------------------------------------------------------------------------------------------------------------
// Layer resource access layer.
//----------------------------------------------------------------------------------------------------------------------

// Models
import { Layer, LayerType } from '../../common/models/layer.js';

// Utils
import { getDB } from '../utils/database.js';
import { NotFoundError } from '../errors.js';

//----------------------------------------------------------------------------------------------------------------------

class LayerResourceAccess
{
    private _toDB(layer : Layer) : Record<string, unknown>
    {
        return {
            name: layer.name,
            description: layer.description,
            features: JSON.stringify(layer.features),
            style: JSON.stringify(layer.style),
        };
    }

    private _fromDB(record : Record<string, unknown>) : Layer
    {
        return {
            name: record.name as string,
            type: record.type as LayerType,
            description: record.description as string,
            features: JSON.parse(record.features as string),
            style: JSON.parse(record.style as string),
        };
    }

    public async exists(name : string) : Promise<boolean>
    {
        const db = await getDB();
        const record = await db('layer')
            .where({ name })
            .first();

        return !!record;
    }

    public async get(name : string) : Promise<Layer>
    {
        const db = await getDB();
        const record = await db('layer')
            .where({ name })
            .first();

        if(!record)
        {
            throw new NotFoundError(`Layer with name "${ name }" not found.`);
        }

        return this._fromDB(record);
    }

    public async list() : Promise<Layer[]>
    {
        const db = await getDB();
        const records = await db('layer');

        return records.map(this._fromDB);
    }

    public async add(layer : Layer) : Promise<void>
    {
        const db = await getDB();
        await db('layer').insert(this._toDB(layer));
    }

    public async update(layer : Layer) : Promise<void>
    {
        const db = await getDB();
        await db('layer')
            .where({ name: layer.name })
            .update(this._toDB(layer));
    }

    public async delete(name : string) : Promise<void>
    {
        const db = await getDB();
        await db('layer')
            .where({ name })
            .delete();
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new LayerResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
