//----------------------------------------------------------------------------------------------------------------------
// AdminResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import configUtil from '@strata-js/util-config';

// Interfaces
import { ServerConfig } from '../../common/interfaces/config.js';

//----------------------------------------------------------------------------------------------------------------------

class AdminResourceAccess
{
    isAdmin(email : string) : boolean
    {
        const config = configUtil.get<ServerConfig>();
        return config.auth.users?.admins.includes(email);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new AdminResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
