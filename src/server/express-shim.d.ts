// ---------------------------------------------------------------------------------------------------------------------
// Custom type extensions
// ---------------------------------------------------------------------------------------------------------------------

// Models
import { Account } from '../common/models/account.js';

// ---------------------------------------------------------------------------------------------------------------------

declare global
{
    export namespace Express
    {
        // eslint-disable-next-line @typescript-eslint/no-empty-interface
        // eslint-disable-next-line @typescript-eslint/no-empty-object-type
        export interface User extends Account
        {
        }
        export interface Request
        {
            user ?: User;
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
