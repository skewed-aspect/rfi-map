// ---------------------------------------------------------------------------------------------------------------------
// Route Utils
// ---------------------------------------------------------------------------------------------------------------------

import { resolve } from 'node:path';
import { createReadStream } from 'node:fs';

import { BasicLogger } from '@strata-js/util-logging';
import { NextFunction, Request, Response } from 'express';

// ---------------------------------------------------------------------------------------------------------------------

export type MiddlewareFunction = (request : Request, response : Response, next : NextFunction) => void;
export type ErrorMiddlewareFunction
    = (error : Error, request : Request, response : Response, next : NextFunction) => void;
export type JsonHandlerFunction = (request : Request, response : Response, next ?: NextFunction) => Promise<unknown>;

// ---------------------------------------------------------------------------------------------------------------------

/**
 *  This is just a simple check for authentication that assumes a `isAuthenticated` function exists on the request.
 *  (This is a standard convention that authentication libraries like passport use.)
 **/
function $checkAuth(request : Request) : boolean
{
    if(request['isAuthenticated'] && typeof request['isAuthenticated'] === 'function')
    {
        return request['isAuthenticated']();
    }
    else
    {
        return false;
    }
}

// ---------------------------------------------------------------------------------------------------------------------

/**
 * Basic request logging
 *
 * @param logger - The logger to use.
 *
 * @returns Returns a middleware function to perform logging.
 */
export function requestLogger(logger : BasicLogger) : MiddlewareFunction
{
    return (request, _response, next) =>
    {
        logger.debug(`${ request.method } '${ request.url }'`);
        next();
    };
}

/**
 * Basic error logging
 *
 * @param logger - The logger to use.
 *
 * @returns Returns a middleware function to perform logging.
 */
export function errorLogger(logger : BasicLogger) : ErrorMiddlewareFunction
{
    return (error, request, response, next) =>
    {
        if(response.statusCode < 500)
        {
            logger.warn(`${ request.method } ${ response.statusCode } '${ request.url }': ${ error.stack }`);
        }
        else
        {
            logger.error(`${ request.method } ${ response.statusCode } '${ request.url }': ${ error.stack }`);
        }

        next(error);
    };
}

/**
 * Serves index page.
 *
 * @param _request - Express request.
 * @param response - Express response.
 */
export function serveIndex(_request : Request, response : Response) : void
{
    response.setHeader('Content-Type', 'text/html');
    createReadStream(resolve(import.meta.dirname, '..', '..', '..', 'client', 'index.html')).pipe(response);
}

/**
 * Either serve 'index.html', or run json handler
 *
 * @param response - Express response.
 * @param jsonHandler - Handler function for the json portion of the request.
 * @param skipAuthCheck - Should we skip checking authentication?
 */
export function interceptHTML(response : Response, jsonHandler : JsonHandlerFunction, skipAuthCheck = false) : void
{
    response.format({
        html: serveIndex,
        json(request : Request, resp : Response, next : NextFunction)
        {
            if(!skipAuthCheck || $checkAuth(request))
            {
                Promise.resolve(jsonHandler(request, resp)).catch(next);
            }
            else
            {
                resp.status(401).json({
                    name: 'NotAuthorized',
                    message: `Not authorized.`,
                });
            }
        },
    });
}

/**
 * Ensures that the user is authenticated, or it returns a 401.
 *
 * @param request - Express request.
 * @param response - Express response.
 * @param next - Express next function.
 */
export function ensureAuthenticated(request : Request, response : Response, next : NextFunction) : void
{
    if($checkAuth(request))
    {
        next();
    }
    else
    {
        response.status(401).json({
            name: 'NotAuthorized',
            message: `Not authorized.`,
        });
    }
}

// ---------------------------------------------------------------------------------------------------------------------
