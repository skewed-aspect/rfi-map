//----------------------------------------------------------------------------------------------------------------------
// Auth Router
//----------------------------------------------------------------------------------------------------------------------

import express from 'express';
import passport from 'passport';

//----------------------------------------------------------------------------------------------------------------------

const router = express.Router();

//----------------------------------------------------------------------------------------------------------------------
// Note: The Google Auth strategy is defined in `src/server/auth/google.ts`. It needs to be initialized in the server,
// not here. Look in the `server.ts` file for that.
//----------------------------------------------------------------------------------------------------------------------

// Authenticate
router.get('/google', passport.authenticate('google'));

// Redirect
router.get('/google/redirect', passport.authenticate('google', {
    successReturnToOrRedirect: '/',
    failWithError: true,
}));

// Get Current User
router.get('/user', (req, resp) =>
{
    resp.json(req.user);
});

// Logout endpoint
router.post('/logout', (req, res, done) =>
{
    req.logout(done);
    res.end();
});

//----------------------------------------------------------------------------------------------------------------------

export default router;

//----------------------------------------------------------------------------------------------------------------------
