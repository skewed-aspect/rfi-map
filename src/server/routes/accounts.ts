//----------------------------------------------------------------------------------------------------------------------
// Routes for Accounts
//----------------------------------------------------------------------------------------------------------------------

import express from 'express';

import { ensureAuthenticated } from './utils/index.js';

// Managers
import accountMan from '../managers/account.js';

// Engines
import accountEngine from '../engines/account.js';

//----------------------------------------------------------------------------------------------------------------------

const router = express.Router();

//----------------------------------------------------------------------------------------------------------------------

router.get('/:accountID', ensureAuthenticated, async(req, resp) =>
{
    const user = req.user;

    if(!user.isAdmin && user.id !== req.params.accountID)
    {
        resp.status(403).json({ error: 'You are not authorized to view this account.' });
        return;
    }

    resp.json(await accountMan.getByID(req.params.accountID));
});

router.patch('/:accountID/settings', ensureAuthenticated, async(req, resp) =>
{
    const accountID = req.params.accountID;

    // Validate the settings
    const settings = accountEngine.validateSettings(req.body);

    // Update the settings
    await accountMan.updateSettings(accountID, settings);

    // Return updated account
    return resp.json(await accountMan.getByID(accountID));
});

//----------------------------------------------------------------------------------------------------------------------

export default router;

//----------------------------------------------------------------------------------------------------------------------
