//----------------------------------------------------------------------------------------------------------------------
// Routes for Layers
//----------------------------------------------------------------------------------------------------------------------

import express from 'express';

import { ensureAuthenticated } from './utils/index.js';

// Managers
import layerMan from '../managers/layer.js';

//----------------------------------------------------------------------------------------------------------------------

const router = express.Router();

//----------------------------------------------------------------------------------------------------------------------

router.get('/', async(req, resp) =>
{
    resp.json(await layerMan.list());
});

router.get('/:layerName', async(req, resp) =>
{
    resp.json(await layerMan.get(req.params.layerName));
});

router.put('/:layerName'/* , ensureAuthenticated */, async(req, resp) =>
{
    await layerMan.upsert(req.body);
    resp.json(await layerMan.get(req.params.layerName));
});

router.patch('/:layerName', ensureAuthenticated, async(req, resp) =>
{
    await layerMan.upsert(req.body);
    resp.json(await layerMan.get(req.params.layerName));
});

router.delete('/:layerName', ensureAuthenticated, async(req, resp) =>
{
    await layerMan.delete(req.params.layerName);
    resp.end();
});

//----------------------------------------------------------------------------------------------------------------------

export default router;

//----------------------------------------------------------------------------------------------------------------------
