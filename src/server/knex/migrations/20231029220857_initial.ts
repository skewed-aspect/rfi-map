//----------------------------------------------------------------------------------------------------------------------
// Initial Migration
//----------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';

//----------------------------------------------------------------------------------------------------------------------

export async function up(knex : Knex) : Promise<Knex.QueryBuilder>
{
    // The `account` table
    await knex.schema.createTable('account', (table) =>
    {
        table.string('account_id', 10).primary();
        table.text('email').notNullable()
            .unique()
            .index();
        table.text('name');
        table.text('avatar');
        table.json('settings').notNullable()
            .defaultTo('{}');
        table.timestamp('created').notNullable()
            .defaultTo(knex.fn.now());
        table.timestamp('updated').notNullable()
            .defaultTo(knex.fn.now());
    });
}

//----------------------------------------------------------------------------------------------------------------------

export async function down(knex : Knex) : Promise<Knex.QueryBuilder>
{
    await knex.schema.dropTable('account');
}

//----------------------------------------------------------------------------------------------------------------------
