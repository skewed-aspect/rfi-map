//----------------------------------------------------------------------------------------------------------------------
// Add Layers
//----------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';

import { validLayerTypes } from '../../../common/models/layer.js';

//----------------------------------------------------------------------------------------------------------------------

export async function up(knex : Knex) : Promise<Knex.QueryBuilder>
{
    // The `layer` table
    await knex.schema.createTable('layer', (table) =>
    {
        table.string('name', 100).primary();
        table.text('description');
        table.enum('type', validLayerTypes).notNullable()
            .defaultTo('poly');
        table.json('features').notNullable()
            .defaultTo('{}');
        table.json('style').notNullable()
            .defaultTo('{}');
        table.timestamp('created').notNullable()
            .defaultTo(knex.fn.now());
        table.timestamp('updated').notNullable()
            .defaultTo(knex.fn.now());
    });
}

//----------------------------------------------------------------------------------------------------------------------

export async function down(knex : Knex) : Promise<Knex.QueryBuilder>
{
    await knex.schema.dropTable('layer');
}

//----------------------------------------------------------------------------------------------------------------------
