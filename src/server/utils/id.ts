// ---------------------------------------------------------------------------------------------------------------------
// Utility functions for generating IDs.
// ---------------------------------------------------------------------------------------------------------------------

import { customAlphabet } from 'nanoid';
import { alphanumeric } from 'nanoid-dictionary';

//----------------------------------------------------------------------------------------------------------------------

const shortNano = customAlphabet(alphanumeric, 10);
const longNano = customAlphabet(alphanumeric, 21);

//----------------------------------------------------------------------------------------------------------------------

/**
 * This generates short ids (ex: 'kjAuMBy1ck') that are suitable for a low number of IDs.
 * ~15 years or 129M IDs needed, in order to have a 1% probability of at least one collision at 1000 IDs/hour.
 *
 * @returns Returns a unique string id.
 */
export function shortID() : string
{
    return shortNano();
}

/**
 * This generates long ids (ex: 'JZNvfTPVPPkJo5r56p2Wq') that are suitable for a high number of IDs.
 * ~107 billion years or 936,953T IDs needed, in order to have a 1% probability of at least one collision at
 * 1000 IDs/hour.
 *
 * @returns Returns a unique string id.
 */
export function longID() : string
{
    return longNano();
}

// ---------------------------------------------------------------------------------------------------------------------
