//----------------------------------------------------------------------------------------------------------------------
// GeoJSON Utilities
//----------------------------------------------------------------------------------------------------------------------

import { Feature, Point } from 'geojson';

//----------------------------------------------------------------------------------------------------------------------

export function makePoint(x : number, y : number) : Point
{
    return {
        type: 'Point',
        coordinates: [ x, y ],
    };
}

export function makePointFeature(x : number, y : number, properties : Record<string, any> = {}) : Feature<Point>
{
    return {
        type: 'Feature',
        geometry: makePoint(x, y),
        properties,
    };
}

//----------------------------------------------------------------------------------------------------------------------
