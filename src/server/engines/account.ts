// ---------------------------------------------------------------------------------------------------------------------
// Account Engine
// ---------------------------------------------------------------------------------------------------------------------

import IdentIcon from 'identicon.js';

// Models
import { Account, AccountSettings } from '../../common/models/account.js';

// Utils
import { shortID } from '../utils/id.js';

// ---------------------------------------------------------------------------------------------------------------------

class AccountEngine
{
    private _nameFromEmail(email : string) : string
    {
        return email.split('@')[0]
            .replace('.', ' ')
            .split(' ')
            .map((word) => word[0].toUpperCase() + word.slice(1))
            .join(' ');
    }

    private _buildIdentIconUrl(seed ?: string) : string
    {
        const options = {
            size: 420,
            format: 'svg' as const,
        };
        const icon = new IdentIcon.default(seed || shortID(), options);

        return `data:image/svg+xml;base64,${ icon.toString() }`;
    }

    // -----------------------------------------------------------------------------------------------------------------

    async addAccount(email : string, name ?: string, avatar ?: string) : Promise<Account>
    {
        name = name || this._nameFromEmail(email);

        const id = shortID();
        return {
            id,
            email,
            name,
            avatar: avatar || this._buildIdentIconUrl(id),
            settings: {
                nickName: name,
            },
            isAdmin: false,
        };
    }

    async updateAccount(
        account : Account,
        email : string,
        name ?: string,
        avatar ?: string,
        settings ?: AccountSettings
    ) : Promise<Account>
    {
        return {
            ...account,
            name: name || account.name || this._nameFromEmail(email),
            avatar: avatar || account.avatar || this._buildIdentIconUrl(account.id),
            settings: settings || account.settings,
        };
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Validation
    // -----------------------------------------------------------------------------------------------------------------

    validateSettings(settings : Record<string, unknown>) : AccountSettings
    {
        if(settings.nickName && typeof settings.nickName !== 'string')
        {
            settings.nickName = undefined;
        }

        return {
            ...settings,
        };
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new AccountEngine();

// ---------------------------------------------------------------------------------------------------------------------
