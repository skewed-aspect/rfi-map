//----------------------------------------------------------------------------------------------------------------------
// Handles user serialization/deserialization
//----------------------------------------------------------------------------------------------------------------------

import passport from 'passport';

// Models
import { Account } from '../../common/models/account.js';

// Resource Access
import accountRA from '../resource-access/account.js';

//----------------------------------------------------------------------------------------------------------------------

passport.serializeUser(({ id } : Account, done) =>
{
    done(null, id);
});

passport.deserializeUser(async(id : string, done) =>
{
    try
    {
        const account = await accountRA.get(id);
        done(null, account);
    }
    catch (error)
    {
        done(error);
    }
});

//----------------------------------------------------------------------------------------------------------------------
