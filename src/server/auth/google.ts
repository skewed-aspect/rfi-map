//----------------------------------------------------------------------------------------------------------------------
// Google Authentication Support
//----------------------------------------------------------------------------------------------------------------------

import passport from 'passport';
import GoogleStrategy from 'passport-google-oauth20';
import logging from '@strata-js/util-logging';

// We just need to import this somewhere; here makes sense.
import './serialization.js';

// Interfaces
import { ServerConfig } from '../../common/interfaces/config.js';

// Managers
import accountMan from '../managers/account.js';

//----------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('googleAuth');

//----------------------------------------------------------------------------------------------------------------------

export default {
    initialize(serverConfig : ServerConfig, devMode = false) : void
    {
        const config = serverConfig.auth.google;

        const domain = devMode ? `http://localhost:${ serverConfig.http.port }` : process.env['DOMAIN'];
        const callbackURL = `${ domain }/auth/google/redirect`;

        // Build Strategy
        passport.use(new GoogleStrategy(
            {
                clientID: config.clientID,
                clientSecret: config.clientSecret,
                callbackURL,
                scope: [ 'profile', 'email' ],
                state: true,
            },
            async (_accessToken, _refreshToken, profile, done) =>
            {
                try
                {
                    const email = profile.emails[0].value;
                    const photo = profile.photos[0]?.value;

                    const account = await accountMan.addOrUpdateAccount(email, profile.displayName, photo);

                    done(null, account);
                }
                catch (error)
                {
                    logger.error(`Encountered error during authentication:\n${ error.stack }`, error);
                    done(error);
                }
            }
        ));
    },
};

//----------------------------------------------------------------------------------------------------------------------

