// ---------------------------------------------------------------------------------------------------------------------
// Account Manager
// ---------------------------------------------------------------------------------------------------------------------

import { NotFoundError } from '../errors.js';

// Models
import { Account, AccountSettings } from '../../common/models/account.js';

// Engines
import accountEngine from '../engines/account.js';

// Resource Access
import accountRA from '../resource-access/account.js';

// ---------------------------------------------------------------------------------------------------------------------

class AccountManager
{
    async getByID(id : string) : Promise<Account>
    {
        return accountRA.get(id);
    }

    async addOrUpdateAccount(
        email : string,
        name ?: string,
        avatar ?: string,
        settings ?: AccountSettings
    ) : Promise<Account>
    {
        let account : Account | null = null;
        try
        {
            account = await accountRA.getByEmail(email);
        }
        catch (error)
        {
            if(!(error instanceof NotFoundError))
            {
                throw error;
            }
        }

        if(account)
        {
            const newAccount = await accountEngine.updateAccount(account, email, name, avatar, settings);
            await accountRA.update(newAccount);
            return accountRA.get(newAccount.id);
        }
        else
        {
            const newAccount = await accountEngine.addAccount(email, name, avatar);
            await accountRA.add(newAccount);
            return accountRA.get(newAccount.id);
        }
    }

    async updateSettings(accountID : string, settings : AccountSettings) : Promise<void>
    {
        const account = await accountRA.get(accountID);
        const newSettings = {
            ...account.settings,
            ...settings,
        };

        await accountRA.updateSettings(accountID, newSettings);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new AccountManager();

// ---------------------------------------------------------------------------------------------------------------------
