//----------------------------------------------------------------------------------------------------------------------
// Layer Manager
//----------------------------------------------------------------------------------------------------------------------

// Models
import { Layer } from '../../common/models/layer.js';

// Resource Access
import layerRA from '../resource-access/layer.js';

//----------------------------------------------------------------------------------------------------------------------

class LayerManager
{
    public async get(name : string) : Promise<Layer>
    {
        return layerRA.get(name);
    }

    public async list() : Promise<Layer[]>
    {
        return layerRA.list();
    }

    public async add(layer : Layer) : Promise<void>
    {
        await layerRA.add(layer);
    }

    public async update(layer : Layer) : Promise<void>
    {
        await layerRA.update(layer);
    }

    public async upsert(layer : Layer) : Promise<void>
    {
        const exists = await layerRA.exists(layer.name);
        if(exists)
        {
            await this.update(layer);
        }
        else
        {
            await this.add(layer);
        }
    }

    public async delete(name : string) : Promise<void>
    {
        await layerRA.delete(name);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new LayerManager();

//----------------------------------------------------------------------------------------------------------------------
