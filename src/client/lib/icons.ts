// ---------------------------------------------------------------------------------------------------------------------
// FontAwesome Icons
// ---------------------------------------------------------------------------------------------------------------------

// To add icons, simply use the export shorthand and export the icons under any name you'd like, as long as the
// versions from the different styles don't conflict. As long as the icon is exported from this file, it will be added
// to the library and will be made available for use.

// ---------------------------------------------------------------------------------------------------------------------

// Font Awesome Solid
export {
    faBars,
    faHome,
    faMinus,
    faPlus,
} from '@fortawesome/pro-solid-svg-icons';

// Font Awesome Regular
export {
    faCircleQuestion as farCircleQuestion,
} from '@fortawesome/pro-regular-svg-icons';

// Font Awesome Brands
export {
    faGitlab,
} from '@fortawesome/free-brands-svg-icons';

// Font Awesome Duotone
export {
    faArrowPointer as fadArrowPointer,
    faArrowRightFromBracket as fadArrowRightFromBracket,
    faDrawPolygon as fadDrawPolygon,
    faEye as fadEye,
    faEyeSlash as fadEyeSlash,
    faGear as fadGear,
    faLocationDot as fadLocationDot,
    faLifeRing as fadLifeRing,
    faOctagonExclamation as fadOctagonExclamation,
    faRuler as fadRuler,
    faShieldExclamation as fadShieldExclamation,
    faShieldKeyhole as fadShieldKeyhole,
    faSliders as fadSliders,
    faSync as fadSync,
    faTrashCan as fadTrashCan,
} from '@fortawesome/pro-duotone-svg-icons';

// ---------------------------------------------------------------------------------------------------------------------
