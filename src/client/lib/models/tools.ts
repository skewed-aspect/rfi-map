//----------------------------------------------------------------------------------------------------------------------
// tools.ts
//----------------------------------------------------------------------------------------------------------------------

import { Map as OlMap } from 'ol';

export const validMapTools = [ 'normal', 'measure', 'boundary', 'poi' ] as const;
export type MapToolName = typeof validMapTools[number];

export interface MapTool 
{
    name : MapToolName;
    icon : string | string[];
    title : string;
    authRequired : boolean;
    showUI : boolean;

    active : (map : OlMap) => Promise<void>;
    deactivate : (map : OlMap) => Promise<void>;
}

//----------------------------------------------------------------------------------------------------------------------
