//----------------------------------------------------------------------------------------------------------------------
// Ship Models
//----------------------------------------------------------------------------------------------------------------------

export const validDesignations = [ 'CS', 'EX', 'CDS', 'CXS', 'FMS', 'LSC', 'LSS', 'LXS', 'MV', 'TRS' ];
export type ValidDesignation = typeof validDesignations[number];

export interface ShipSpeedRating
{
    max : number;
    cruise : number;
    emergency ?: number; // TODO: Make this required eventually
}

export interface RFIShipClass
{
    name : string;
    acceleration ?: ShipSpeedRating; // TODO: Make this required eventually
    subspaceSpeed : ShipSpeedRating;
}

export interface RFIShip extends RFIShipClass
{
    designation : ValidDesignation | null;
}

//----------------------------------------------------------------------------------------------------------------------
