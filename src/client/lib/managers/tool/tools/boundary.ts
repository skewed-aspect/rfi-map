//----------------------------------------------------------------------------------------------------------------------
// Boundary Tool
//----------------------------------------------------------------------------------------------------------------------

import { Map as OlMap } from 'ol';
import { Draw as OlDraw, Modify as OlModify, Snap as OlSnap } from 'ol/interaction';
import { Vector as VectorLayer } from 'ol/layer';
import { Vector as VectorSource } from 'ol/source';

// Interfaces
import { MapTool } from '../../../models/tools';

// Utils
import styleUtil from '../../../utils/styleUtil';

//----------------------------------------------------------------------------------------------------------------------

export class BoundaryTool implements MapTool
{
    readonly name = 'boundary';
    readonly icon = [ 'fad', 'draw-polygon' ];
    readonly title = 'Boundary Tool';
    readonly authRequired = true;
    readonly showUI = true;

    #olDrawInteraction : OlDraw = null;
    #olModifyInteraction : OlModify = null;
    #olSnapInteraction : OlSnap = null;

    #featureLayer = new VectorLayer<VectorSource>({
        source: new VectorSource(),
        style: styleUtil.getStyle('measure'),
    });

    //------------------------------------------------------------------------------------------------------------------

    async active(map : OlMap) : Promise<void>
    {
        this.#featureLayer.setMap(map);

        if(!this.#olDrawInteraction)
        {
            this.#olDrawInteraction = new OlDraw({
                type: 'Polygon',
                source: this.#featureLayer.getSource(),
                // style: styleUtil.getStyle('draw')
            });

            // this.#olDrawInteraction.on('drawend', this._handleDrawEnd.bind(this));
        }

        if(!this.#olModifyInteraction)
        {
            this.#olModifyInteraction = new OlModify({
                source: this.#featureLayer.getSource(),
                style: styleUtil.getStyle('draw'),
            });
        }

        if(!this.#olSnapInteraction)
        {
            this.#olSnapInteraction = new OlSnap({
                source: this.#featureLayer.getSource(),
            });
        }

        // Add the modify interaction to the map
        map.addInteraction(this.#olModifyInteraction);
        this.#olModifyInteraction.setActive(true);

        // Add the draw interaction to the map
        map.addInteraction(this.#olDrawInteraction);
        this.#olDrawInteraction.setActive(true);

        // Add the snap interaction to the map
        map.addInteraction(this.#olSnapInteraction);
        this.#olSnapInteraction.setActive(true);
    }

    //------------------------------------------------------------------------------------------------------------------

    async deactivate(map : OlMap) : Promise<void>
    {
        // Clear the feature source
        this.#featureLayer.getSource().clear();

        // Remove the draw interaction from the map
        this.#olDrawInteraction.setActive(false);
        map.removeInteraction(this.#olDrawInteraction);

        // Remove the modify interaction from the map
        this.#olModifyInteraction.setActive(false);
        map.removeInteraction(this.#olModifyInteraction);

        // Remove the snap interaction from the map
        this.#olSnapInteraction.setActive(false);
        map.removeInteraction(this.#olSnapInteraction);
    }
}

//----------------------------------------------------------------------------------------------------------------------
