//----------------------------------------------------------------------------------------------------------------------
// Measurement Tool
//----------------------------------------------------------------------------------------------------------------------

import { BehaviorSubject, Observable } from 'rxjs';

import { Collection, Feature, Map as OlMap } from 'ol';
import { Vector as VectorLayer } from 'ol/layer';
import { Vector as VectorSource } from 'ol/source';
import { Draw as OlDraw, Modify as OlModify, Snap as OlSnap } from 'ol/interaction';
import { LineString } from 'ol/geom';
import { DrawEvent } from 'ol/interaction/Draw';
import { EventsKey } from 'ol/events';
import { unByKey } from 'ol/Observable';

// Interfaces
import { MapTool } from '../../../models/tools';

// Utils
import { pxToLy } from '../../../utils/unitConversion.js';
import styleUtil from '../../../utils/styleUtil.js';

//----------------------------------------------------------------------------------------------------------------------

export class MeasureTool implements MapTool
{
    readonly name = 'measure';
    readonly icon = [ 'fad', 'ruler' ];
    readonly title = 'Measure Distance Tool';
    readonly authRequired = false;
    readonly showUI = true;

    #olDrawInteraction : OlDraw = null;
    #olModifyInteraction : OlModify = null;
    #olSnapInteraction : OlSnap = null;

    #featureLayer = new VectorLayer<VectorSource>({
        source: new VectorSource(),
        style: styleUtil.getStyle('measure'),
    });

    //------------------------------------------------------------------------------------------------------------------

    _distanceSubject = new BehaviorSubject<number>(0);
    _updateKey : EventsKey = null;

    //------------------------------------------------------------------------------------------------------------------

    get distance$() : Observable<number>
    {
        return this._distanceSubject.asObservable();
    }

    //------------------------------------------------------------------------------------------------------------------
    // Event Handlers
    //------------------------------------------------------------------------------------------------------------------

    _handleDrawEnd(event : DrawEvent) : void
    {
        this.#olDrawInteraction.setActive(false);

        const distance = this.getDistance(event.feature);
        this._distanceSubject.next(distance);

        this._updateKey = event.feature.on('change', () =>
        {
            const dist = this.getDistance(event.feature);
            this._distanceSubject.next(dist);
        });
    }

    //------------------------------------------------------------------------------------------------------------------

    async active(map : OlMap) : Promise<void>
    {
        this.#featureLayer.setMap(map);

        if(!this.#olDrawInteraction)
        {
            this.#olDrawInteraction = new OlDraw({
                type: 'LineString',
                source: this.#featureLayer.getSource(),
                style: styleUtil.getStyle('draw'),
            });

            this.#olDrawInteraction.on('drawend', this._handleDrawEnd.bind(this));
        }

        if(!this.#olModifyInteraction)
        {
            this.#olModifyInteraction = new OlModify({
                source: this.#featureLayer.getSource(),
                style: styleUtil.getStyle('draw'),
            });
        }

        if(!this.#olSnapInteraction)
        {
            const poi = map.getLayers()
                .getArray()
                .find((layer) => layer.get('name') === 'poi');

            this.#olSnapInteraction = new OlSnap({
                edge: true,
                vertex: true,
                pixelTolerance: 5,
                features: new Collection([
                    ...(poi as VectorLayer)?.getSource().getFeatures() ?? [],
                ]),
            });
        }

        // Add the draw interaction to the map
        map.addInteraction(this.#olDrawInteraction);
        this.#olDrawInteraction.setActive(true);

        // Add the modify interaction to the map
        map.addInteraction(this.#olModifyInteraction);
        this.#olModifyInteraction.setActive(true);

        // Add the snap interaction to the map
        map.addInteraction(this.#olSnapInteraction);
        this.#olSnapInteraction.setActive(true);
    }

    async deactivate(map : OlMap) : Promise<void>
    {
        // Clear the feature source
        this.#featureLayer.getSource().clear();

        // Remove the draw interaction from the map
        this.#olDrawInteraction.setActive(false);
        map.removeInteraction(this.#olDrawInteraction);

        // Remove the modify interaction from the map
        this.#olModifyInteraction.setActive(false);
        map.removeInteraction(this.#olModifyInteraction);

        // Remove the snap interaction from the map
        this.#olSnapInteraction.setActive(false);
        map.removeInteraction(this.#olSnapInteraction);

        if(this._updateKey)
        {
            unByKey(this._updateKey);
            this._updateKey = null;
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    // Tool Specific Methods
    //------------------------------------------------------------------------------------------------------------------

    getDistance(feature : Feature) : number
    {
        return pxToLy((feature.getGeometry() as LineString).getLength());
    }

    clear() : void
    {
        if(this._updateKey)
        {
            unByKey(this._updateKey);
            this._updateKey = null;
        }

        // Clear the measured distance
        this._distanceSubject.next(0);

        this.#featureLayer.getSource().clear();
        this.#olDrawInteraction.setActive(true);
    }
}

//----------------------------------------------------------------------------------------------------------------------
