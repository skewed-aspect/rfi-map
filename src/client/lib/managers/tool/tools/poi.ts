//----------------------------------------------------------------------------------------------------------------------
// POI Tool
//----------------------------------------------------------------------------------------------------------------------

import { Map as OlMap } from 'ol';

// Interfaces
import { MapTool } from '../../../models/tools';

//----------------------------------------------------------------------------------------------------------------------

export class POITool implements MapTool
{
    readonly name = 'poi';
    readonly icon = [ 'fad', 'location-dot' ];
    readonly title = 'Points of Interest Tool';
    readonly authRequired = true;
    readonly showUI = true;

    //------------------------------------------------------------------------------------------------------------------

    async active(map : OlMap) : Promise<void>
    {
        console.log('Activating POI Tool', map);
    }

    //------------------------------------------------------------------------------------------------------------------

    async deactivate(map : OlMap) : Promise<void>
    {
        console.log('Deactivating POI Tool', map);
    }
}

//----------------------------------------------------------------------------------------------------------------------
