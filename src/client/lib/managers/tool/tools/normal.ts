//----------------------------------------------------------------------------------------------------------------------
// Normal Tool
//----------------------------------------------------------------------------------------------------------------------

import { Map as OlMap } from 'ol';
import { Select as OlSelect } from 'ol/interaction';

// Interfaces
import { MapTool } from '../../../models/tools';

//----------------------------------------------------------------------------------------------------------------------

export class NormalTool implements MapTool
{
    readonly name = 'normal';
    readonly icon = [ 'fad', 'arrow-pointer' ];
    readonly title = 'Normal Tool';
    readonly authRequired = false;
    readonly showUI = false;

    #olSelectInteraction : OlSelect = null;

    //------------------------------------------------------------------------------------------------------------------

    async active(_map : OlMap) : Promise<void>
    {
        // TODO: configure this once I know what I want it to do.
        // if(!this.#olSelectInteraction)
        // {
        //     this.#olSelectInteraction = new OlSelect();
        // }
        //
        // Add the select interaction to the map
        // map.addInteraction(this.#olSelectInteraction);
        // this.#olSelectInteraction.setActive(true);
    }

    //------------------------------------------------------------------------------------------------------------------

    async deactivate(map : OlMap) : Promise<void>
    {
        // Remove the select interaction from the map
        // this.#olSelectInteraction.setActive(false);
        map.removeInteraction(this.#olSelectInteraction);
    }
}

//----------------------------------------------------------------------------------------------------------------------
