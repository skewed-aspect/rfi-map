//----------------------------------------------------------------------------------------------------------------------
// Tool Manager
//----------------------------------------------------------------------------------------------------------------------

import { Map as OlMap } from 'ol';

// Interfaces
import { MapTool, MapToolName } from '../../models/tools';

// Stores
import accountStore from '../../resource-access/store/account.js';
import toolsStore from '../../resource-access/store/tools.js';

// Tools
import { NormalTool } from './tools/normal.js';
import { MeasureTool } from './tools/measure.js';
import { BoundaryTool } from './tools/boundary.js';
import { POITool } from './tools/poi.js';

//----------------------------------------------------------------------------------------------------------------------

export class ToolManager
{
    #initialized = false;
    #olMap : OlMap | null = null;

    //------------------------------------------------------------------------------------------------------------------

    async initialize(map : OlMap) : Promise<void>
    {
        if(!this.#initialized)
        {
            this.#olMap = map;

            // Register tools
            this.registerTool(new NormalTool());
            this.registerTool(new MeasureTool());
            this.registerTool(new BoundaryTool());
            this.registerTool(new POITool());

            // Activate default tool
            await this.activateTool('normal');

            // Mark ourselves as initialized
            this.#initialized = true;
        }
    }

    //------------------------------------------------------------------------------------------------------------------

    registerTool(tool : MapTool) : void
    {
        // Build a list of the current tool names
        const existingToolNames = toolsStore.tools.map((existingTool) => existingTool.name);

        // Add a tool to the list of registered tools, making sure it hasn't already been registered.
        if(!existingToolNames.includes(tool.name))
        {
            toolsStore.setTools([ ...toolsStore.tools, tool ]);
        }
    }

    async activateTool(name : MapToolName) : Promise<void>
    {
        if(this.#olMap)
        {
            const currentTool = toolsStore.current;
            const newTool = toolsStore.tools.find((tool) => tool.name === name);
            if(newTool && newTool.name !== currentTool?.name)
            {
                if(newTool.authRequired && !accountStore.account)
                {
                    console.warn('Cannot activate tool, user is not authenticated.');
                    return;
                }

                if(currentTool)
                {
                    await currentTool.deactivate(this.#olMap);
                }

                toolsStore.setCurrentTool(newTool);
                await newTool.active(this.#olMap);
            }
        }
        else
        {
            console.warn('Cannot activate tool, tool manager is not initialized.');
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new ToolManager();

//----------------------------------------------------------------------------------------------------------------------
