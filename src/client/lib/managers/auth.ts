//----------------------------------------------------------------------------------------------------------------------
// AuthManager
//----------------------------------------------------------------------------------------------------------------------

// Stores
import accountStore from '../resource-access/store/account.js';

// Resource Access
import authRA from '../resource-access/auth.js';

//----------------------------------------------------------------------------------------------------------------------

export class AuthManager
{
    get account$()
    {
        return accountStore.account$;
    }

    async loadCurrentUser() : Promise<void>
    {
        const account = await authRA.getCurrentUser();
        accountStore.setAccount(account);
    }

    async logout() : Promise<void>
    {
        await authRA.logout();
        accountStore.setAccount(null);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new AuthManager();

//----------------------------------------------------------------------------------------------------------------------
