//----------------------------------------------------------------------------------------------------------------------
// ShipManager
//----------------------------------------------------------------------------------------------------------------------

import { Observable } from 'rxjs';

// Models
import { RFIShip, RFIShipClass } from '../models/ships';

// Stores
import shipStore from '../resource-access/store/ships';

//----------------------------------------------------------------------------------------------------------------------

class ShipManager
{
    get classes$() : Observable<RFIShipClass[]>
    {
        return shipStore.classes$;
    }

    get ships$() : Observable<RFIShip[]>
    {
        return shipStore.ships$;
    }

    //------------------------------------------------------------------------------------------------------------------

    async init() : Promise<void>
    {
        shipStore.addClasses([
            {
                name: 'Archigos',
                subspaceSpeed: {
                    max: 1350000,
                    cruise: 1200000,
                },
            },
            {
                name: 'Erebus',
                subspaceSpeed: {
                    max: 1500000,
                    cruise: 1350000,
                },
            },
            {
                name: 'Revelation',
                subspaceSpeed: {
                    max: 2000000,
                    cruise: 1750000,
                },
            },
            {
                name: 'Kali',
                subspaceSpeed: {
                    max: 1350000,
                    cruise: 1200000,
                },
            },
            {
                name: 'Bastion',
                subspaceSpeed: {
                    max: 1650000,
                    cruise: 1500000,
                },
            },
            {
                name: 'Valorous',
                subspaceSpeed: {
                    max: 1400000,
                    cruise: 1250000,
                },
            },
            {
                name: 'Viceroy',
                subspaceSpeed: {
                    max: 780000,
                    cruise: 700000,
                },
            },
        ]);

        shipStore.addShips([
            {
                name: 'Archigos',
                designation: 'LXS',
                subspaceSpeed: {
                    max: 1350000,
                    cruise: 1200000,
                },
            },
            {
                name: 'Chilkoot',
                designation: 'CX',
                subspaceSpeed: {
                    max: 1150000,
                    cruise: 950000,
                },
            },
            {
                name: "Spring's Endeavor",
                designation: 'FMS',
                subspaceSpeed: {
                    max: 1200000,
                    cruise: 1100000,
                },
            },
        ]);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new ShipManager();

//----------------------------------------------------------------------------------------------------------------------
