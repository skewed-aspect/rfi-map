//----------------------------------------------------------------------------------------------------------------------
// Layer Manager
//----------------------------------------------------------------------------------------------------------------------

// Engines
import layerEngine from '../engines/layer.js';

// Models
import { Layer } from '../../../common/models/layer.js';

// Stores
import layerStore from '../resource-access/store/layers.js';
import olStore from '../resource-access/store/openlayers.js';

// Resource Access
import layerRA from '../resource-access/layer.js';

//----------------------------------------------------------------------------------------------------------------------

class LayerManager
{
    #defaultLayers : string[] = [
        'major-boundaries',
        'terran-boundaries',
        'freelance-boundaries',
        'league-boundaries',
        'noraellian-boundaries',
        'poi'
    ];

    public async init() : Promise<void>
    {
        // Get a list of all available layers
        await this.list()
            .catch((error) => console.error('Failed to list layers:', error));

        olStore.mainMap$.subscribe((map) =>
        {
            if(map)
            {
                console.log('Adding default layers to map...');

                // Add default layers to the map
                for(const layerName of this.#defaultLayers)
                {
                    const layer = layerStore.allLayers.find((l) => l.name === layerName);
                    if(layer)
                    {
                        console.log(`Adding default layer: ${ layer.name }`);
                        this.addLayerToMap(layer);
                    }
                }
            }
        });
    }

    public isDefaultLayer(layerName : string) : boolean
    {
        return this.#defaultLayers.includes(layerName);
    }

    public addLayerToMap(layer : Layer) : void
    {
        const map = olStore.mainMap;
        const alreadyAdded = layerStore.addedLayers.find((addedLayer) => addedLayer.name === layer.name);

        if(!alreadyAdded)
        {
            layerStore.setAddedLayers([ ...layerStore.addedLayers, layer ]);
            if(map)
            {
                const vector = layerEngine.buildLayer(layer);
                map.addLayer(vector);
            }
        }
    }

    public removeLayerFromMap(layer : Layer) : void
    {
        const map = olStore.mainMap;
        const index = layerStore.addedLayers.findIndex((addedLayer) => addedLayer.name === layer.name);
        if(index !== -1)
        {
            // Splice the item out of the array
            layerStore.addedLayers.splice(index, 1);

            // Still do a set operation to trigger the observable
            layerStore.setAddedLayers([ ...layerStore.addedLayers ]);
            if(map)
            {
                map.removeLayer(olStore.vectorLayers.get(layer.name));
            }
        }
    }

    public getVisibility(layer : Layer) : boolean
    {
        const vectorLayer = olStore.vectorLayers.get(layer.name);
        if(vectorLayer)
        {
            return vectorLayer.getVisible();
        }
        return false;
    }

    public toggleVisibility(layer : Layer) : void
    {
        const vectorLayer = olStore.vectorLayers.get(layer.name);
        if(vectorLayer)
        {
            vectorLayer.setVisible(!vectorLayer.getVisible());
        }
    }

    public async list() : Promise<void>
    {
        const layers = await layerRA.list();
        layerStore.setAllLayers(layers);
    }

    // public async add(layer : Layer) : Promise<void>
    // {
    //     await layerRA.add(layer);
    // }
    //
    // public async update(layer : Layer) : Promise<void>
    // {
    //     await layerRA.update(layer);
    // }
    //
    // public async upsert(layer : Layer) : Promise<void>
    // {
    //     const exists = await layerRA.exists(layer.name);
    //     if(exists)
    //     {
    //         await this.update(layer);
    //     }
    //     else
    //     {
    //         await this.add(layer);
    //     }
    // }
    //
    // public async delete(name : string) : Promise<void>
    // {
    //     await layerRA.delete(name);
    // }
}

//----------------------------------------------------------------------------------------------------------------------

export default new LayerManager();

//----------------------------------------------------------------------------------------------------------------------
