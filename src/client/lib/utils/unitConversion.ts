// ---------------------------------------------------------------------------------------------------------------------
// Unit Conversion Utilities
// ---------------------------------------------------------------------------------------------------------------------

export function pxToLy(px : number) : number
{
    // Convert to light years from map pixels. This is only true for this _specific_ map image.
    // (1.25 is a fudge factor to calibrate the scale to the screen.)
    return px * 20 * 1.25;
}

/**
 * Takes seconds and returns a decimal number of days.
 *
 * @param seconds - The seconds to convert.
 *
 * @returns Returns the time as a decimal number of days.
 **/
export function secondsToDays(seconds : number) : number
{
    return seconds / 60 / 60 / 24;
}

/**
 * Takes a number in seconds and formats it as a string in years, months, days, hours, minutes, and seconds. If a
 * value is 0, it is not included.
 *
 * @param seconds - The number of seconds to format.
 *
 * @returns Returns a string in the format "X years, Y months, Z days, A hours, B minutes, C seconds".
 **/
export function formatTime(seconds : number) : string
{
    const years = Math.floor(seconds / 60 / 60 / 24 / 365);
    const months = Math.floor(seconds / 60 / 60 / 24 / 30.436875) % 12;
    const days = Math.floor(seconds / 60 / 60 / 24) % 30;
    const hours = Math.floor(seconds / 60 / 60) % 24;
    const minutes = Math.floor(seconds / 60) % 60;
    const secs = Math.floor(seconds) % 60;

    let time = '';
    if(years > 0) { time += `${ years } years, `; }
    if(months > 0) { time += `${ months } months, `; }
    if(days > 0) { time += `${ days } days, `; }
    if(hours > 0) { time += `${ hours } hours, `; }
    if(minutes > 0) { time += `${ minutes } minutes, `; }
    if(secs > 0) { time += `${ secs } seconds`; }

    return time;
}

// ---------------------------------------------------------------------------------------------------------------------
