//----------------------------------------------------------------------------------------------------------------------
// Subspace Tools
//----------------------------------------------------------------------------------------------------------------------

/**
 * Takes in the distance in light years, and the speed as a multiple of the speed of light and returns the time in
 * seconds.
 *
 * @param lightYears - The distance in light years.
 * @param speedOfLightMult - The speed as a multiple of `c`.
 *
 * @returns Returns the time in seconds.
 **/
export function subspaceTime(lightYears : number, speedOfLightMult : number) : number
{
    // Constants
    const speedOfLight = 299792458;
    const metersPerLY = 9461000000000000;

    // Calculations
    const distanceMeters = lightYears * metersPerLY;
    const speed = speedOfLightMult * speedOfLight;
    return distanceMeters / speed;
}

//----------------------------------------------------------------------------------------------------------------------
