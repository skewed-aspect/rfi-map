//----------------------------------------------------------------------------------------------------------------------
// StyleUtil
//----------------------------------------------------------------------------------------------------------------------

import { FlatStyleLike } from 'ol/style/flat';
import { Style } from 'ol/style';
import VectorLayer from 'ol/layer/Vector';
import OlFeature from 'ol/Feature';

//----------------------------------------------------------------------------------------------------------------------

class StyleUtil
{
    #style = {
        draw: [
            {
                'stroke-color': 'white',
                'stroke-width': 5,
                'circle-fill-color': 'white',
                'circle-radius': 7,
                'point-radius': 7,
                'point-fill-color': 'white',
            },
            {
                'stroke-color': '#8348e8',
                'stroke-width': 3,
                'circle-fill-color': '#8348e8',
                'circle-radius': 5,
                'point-radius': 5,
                'point-fill-color': '#8348e8',
            },
        ],
        measure: {
            'fill-color': 'rgba(255, 255, 255, 0.2)',
            'stroke-color': '#ff9900',
            'stroke-width': 3,
            'circle-radius': 7,
            'circle-fill-color': '#ffcc33',
        },
    };

    getStyle(styleName : string) : FlatStyleLike
    {
        return this.#style[styleName];
    }

    /**
     * Convert a flat style to a classic OpenLayers style
     * @param flatStyle The flat style to convert to
     * @returns The classic version of the style
     */
    toStyle(flatStyle : FlatStyleLike) : Style[] | Style | undefined
    {
        const vectorLayer = new VectorLayer({ style: flatStyle });
        if(!vectorLayer) { return; }
        const styleFunction = vectorLayer.getStyleFunction();
        if(!styleFunction) { return; }
        const newFeature = new OlFeature();
        const fakeResolution = 1;
        const styleResponse = styleFunction(newFeature, fakeResolution);
        if(!styleResponse) { return; }

        if(Array.isArray(styleResponse))
        {
            return styleResponse?.length > 1 ? styleResponse : styleResponse[0];
        }
        else
        {
            return styleResponse;
        }
    };
}

//----------------------------------------------------------------------------------------------------------------------

export default new StyleUtil();

//----------------------------------------------------------------------------------------------------------------------
