// ---------------------------------------------------------------------------------------------------------------------
// Layer Engine
// ---------------------------------------------------------------------------------------------------------------------

import { GeoJSON } from 'ol/format';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { Feature } from 'ol';
import { Style } from 'ol/style';

// Models
import { Layer } from '../../../common/models/layer.js';

// Stores
import olStore from '../resource-access/store/openlayers.js';

// Utils
import styleUtil from '../utils/styleUtil.js';

// ---------------------------------------------------------------------------------------------------------------------

const geoJsonFormat = new GeoJSON();

// ---------------------------------------------------------------------------------------------------------------------

class LayerEngine
{
    buildLayer(layer : Layer) : VectorLayer
    {
        let olLayer = olStore.vectorLayers.get(layer.name);
        if(!olLayer)
        {
            const style = Object.keys(layer.style).length === 0 ? undefined : layer.style;

            const styleFunction = (feature : Feature, _resolution : number) : Style | Style[] =>
            {
                const flatStyle = feature.get('style') ?? style;
                return styleUtil.toStyle([
                    flatStyle ?? {},
                    {
                        'text-value': feature.get('name'),
                        'text-font': 'bold 16px TrebuchetMS, Trebuchet MS, Verdana, sans-serif',
                        'text-fill-color': '#555',
                        'text-stroke-color': '#FFF',
                        'text-stroke-width': 2,
                        'text-offset-y': -15,
                    },
                ]);
            };

            olLayer = new VectorLayer({
                source: new VectorSource(),
                style: styleFunction,
                className: layer.name,
                properties: {
                    name: layer.name,
                    type: layer.type,
                },
            });

            olStore.addVectorLayer(layer, olLayer);
        }
        else
        {
            // Clear out the source
            olLayer.getSource().clear();
        }

        const olFeatures = layer.features.map((feature) => geoJsonFormat.readFeature(feature));
        olFeatures.forEach((feature) => olLayer.getSource().addFeature(feature as any));

        return olLayer;
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new LayerEngine();

// ---------------------------------------------------------------------------------------------------------------------
