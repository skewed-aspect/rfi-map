//----------------------------------------------------------------------------------------------------------------------
// OpenOpenLayers Store
//----------------------------------------------------------------------------------------------------------------------

import { BehaviorSubject, Observable } from 'rxjs';

import { Map as OlMap } from 'ol';
import VectorLayer from 'ol/layer/Vector';
import { Layer } from '../../../../common/models/layer.js';

// ---------------------------------------------------------------------------------------------------------------------

class OpenLayersStore
{
    // Store off the openlayers layers, so we can work with them directly as needed
    private readonly _vectorLayers = new Map<string, VectorLayer>();

    // Reactive Subjects
    private readonly _mainMapSubject = new BehaviorSubject<OlMap | null>(null);

    get vectorLayers() : Map<string, VectorLayer> { return this._vectorLayers; }

    //------------------------------------------------------------------------------------------------------------------

    get mainMap$() : Observable<OlMap | null> { return this._mainMapSubject.asObservable(); }

    get mainMap() : OlMap | null { return this._mainMapSubject.getValue(); }

    //------------------------------------------------------------------------------------------------------------------

    setMainMap(map : OlMap | null) : void
    {
        this._mainMapSubject.next(map);
    }

    addVectorLayer(layer : Layer, vectorLayer : VectorLayer) : void
    {
        this._vectorLayers.set(layer.name, vectorLayer);
    }

    removeVectorLayer(layer : Layer) : void
    {
        this._vectorLayers.delete(layer.name);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new OpenLayersStore();

//----------------------------------------------------------------------------------------------------------------------
