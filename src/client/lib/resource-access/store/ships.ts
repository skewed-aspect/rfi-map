//----------------------------------------------------------------------------------------------------------------------
// Ships Store
//----------------------------------------------------------------------------------------------------------------------

import { BehaviorSubject, Observable } from 'rxjs';

// Models
import { RFIShip, RFIShipClass } from '../../models/ships';

// ---------------------------------------------------------------------------------------------------------------------

class ShipStore
{
    private readonly _shipsSubject = new BehaviorSubject<RFIShip[]>([]);
    private readonly _shipClassesSubject = new BehaviorSubject<RFIShipClass[]>([]);

    //------------------------------------------------------------------------------------------------------------------

    get classes$() : Observable<RFIShipClass[]> { return this._shipClassesSubject.asObservable(); }
    get classes() : RFIShipClass[] { return this._shipClassesSubject.getValue(); }

    get ships$() : Observable<RFIShip[]> { return this._shipsSubject.asObservable(); }
    get ships() : RFIShip[] { return this._shipsSubject.getValue(); }

    //------------------------------------------------------------------------------------------------------------------

    addClasses(shipClasses : RFIShipClass | RFIShipClass[]) : void
    {
        if(Array.isArray(shipClasses))
        {
            this._shipClassesSubject.next([ ...this._shipClassesSubject.getValue(), ...shipClasses ]);
        }
        else
        {
            this._shipClassesSubject.next([ ...this._shipClassesSubject.getValue(), shipClasses ]);
        }
    }

    addShips(ships : RFIShip | RFIShip[]) : void
    {
        if(Array.isArray(ships))
        {
            this._shipsSubject.next([ ...this._shipsSubject.getValue(), ...ships ]);
        }
        else
        {
            this._shipsSubject.next([ ...this._shipsSubject.getValue(), ships ]);
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new ShipStore();

//----------------------------------------------------------------------------------------------------------------------
