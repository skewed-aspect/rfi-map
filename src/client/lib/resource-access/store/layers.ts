// ---------------------------------------------------------------------------------------------------------------------
// Layer Store
// ---------------------------------------------------------------------------------------------------------------------

import { BehaviorSubject, Observable } from 'rxjs';

// Interfaces
import { Layer } from '../../../../common/models/layer.js';

// ---------------------------------------------------------------------------------------------------------------------

class LayerStore
{
    // Reactive Subjects
    private readonly _allLayersSubject = new BehaviorSubject<Layer[]>([]);
    private readonly _addedLayersSubject = new BehaviorSubject<Layer[]>([]);

    //------------------------------------------------------------------------------------------------------------------

    get allLayers$() : Observable<Layer[]> { return this._allLayersSubject.asObservable(); }
    get addedLayers$() : Observable<Layer[]> { return this._addedLayersSubject.asObservable(); }

    get allLayers() : Layer[] { return this._allLayersSubject.getValue(); }
    get addedLayers() : Layer[] { return this._addedLayersSubject.getValue(); }

    //------------------------------------------------------------------------------------------------------------------

    setAllLayers(layers : Layer[]) : void
    {
        this._allLayersSubject.next(layers);
    }

    setAddedLayers(layers : Layer[]) : void
    {
        this._addedLayersSubject.next(layers);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new LayerStore();

// ---------------------------------------------------------------------------------------------------------------------
