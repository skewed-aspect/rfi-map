//----------------------------------------------------------------------------------------------------------------------
// Tools Store
//----------------------------------------------------------------------------------------------------------------------

import { BehaviorSubject, Observable } from 'rxjs';

// Interfaces
import { MapTool } from '../../models/tools';

//----------------------------------------------------------------------------------------------------------------------

class MapToolStore
{
    private readonly _currentToolSubject = new BehaviorSubject<MapTool | null>(null);
    private readonly _registeredToolsSubject = new BehaviorSubject<MapTool[]>([]);

    //------------------------------------------------------------------------------------------------------------------

    get tools$() : Observable<MapTool[]> { return this._registeredToolsSubject.asObservable(); }
    get current$() : Observable<MapTool | null> { return this._currentToolSubject.asObservable(); }

    get tools() : MapTool[] { return this._registeredToolsSubject.getValue(); }
    get current() : MapTool | null { return this._currentToolSubject.getValue(); }

    //------------------------------------------------------------------------------------------------------------------

    setCurrentTool(tool : MapTool) : void
    {
        this._currentToolSubject.next(tool);
    }

    setTools(tools : MapTool[]) : void
    {
        this._registeredToolsSubject.next(tools);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new MapToolStore();

//----------------------------------------------------------------------------------------------------------------------
