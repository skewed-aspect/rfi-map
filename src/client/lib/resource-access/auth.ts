//----------------------------------------------------------------------------------------------------------------------
// AuthResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import axios from 'axios';

// Models
import { Account } from '../../../common/models/account.js';

//----------------------------------------------------------------------------------------------------------------------

class AuthResourceAccess
{
    async getCurrentUser() : Promise<Account | null>
    {
        try
        {
            const { data } = await axios.get('/auth/user', { withCredentials: true });
            return data as Account;
        }
        catch (_error)
        {
            return null;
        }
    }

    async logout() : Promise<void>
    {
        await axios.post('/auth/logout');
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new AuthResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
