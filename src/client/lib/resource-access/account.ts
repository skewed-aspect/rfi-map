//----------------------------------------------------------------------------------------------------------------------
// AccountResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import axios from 'axios';

// Models
import { Account } from '../../../common/models/account.js';

//----------------------------------------------------------------------------------------------------------------------

class AccountResourceAccess
{
    async getAccount(id : string) : Promise<Account | null>
    {
        try
        {
            const response = await axios.get(`/account/${ id }`);
            return response.data;
        }
        catch (error)
        {
            if(error.response?.status === 404)
            {
                return null;
            }

            return error;
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new AccountResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
