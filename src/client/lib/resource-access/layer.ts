//----------------------------------------------------------------------------------------------------------------------
// Layers Resource Access
//----------------------------------------------------------------------------------------------------------------------

import axios from 'axios';

// Models
import { Layer } from '../../../common/models/layer.js';

//----------------------------------------------------------------------------------------------------------------------

class LayerResourceAccess
{
    async get(name : string) : Promise<Layer | null>
    {
        try
        {
            const response = await axios.get(`/layer/${ name }`);
            return response.data;
        }
        catch (error)
        {
            if(error.response?.status === 404)
            {
                return null;
            }

            return error;
        }
    }

    async list() : Promise<Layer[]>
    {
        const response = await axios.get('/layer');
        return response.data;
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new LayerResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
