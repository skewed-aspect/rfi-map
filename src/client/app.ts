// ---------------------------------------------------------------------------------------------------------------------
// Main App
// ---------------------------------------------------------------------------------------------------------------------

import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import { createBootstrap } from 'bootstrap-vue-next';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

// Managers
import authMan from './lib/managers/auth.js';
import layerMan from './lib/managers/layer';
import shipMan from './lib/managers/ship.js';

// Components
import AppComponent from './app.vue';

// Pages
import MainPage from './pages/mainPage.vue';
import OpenLayersMap from 'vue3-openlayers';

// Site Style
import './scss/theme.scss';

// Site Icons
import * as icons from './lib/icons.js';

// ---------------------------------------------------------------------------------------------------------------------
// Vue Router
// ---------------------------------------------------------------------------------------------------------------------

const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', name: 'home', component: MainPage },

        // Fallback to home
        { path: '/:catchAll(.*)', redirect: { name: 'home' } },
    ],
});

// ---------------------------------------------------------------------------------------------------------------------
// Vue App
// ---------------------------------------------------------------------------------------------------------------------

const app = createApp(AppComponent);

// Set up Bootstrap Vue
app.use(createBootstrap());

// Add icons to the library
library.add(icons);

// Create the font awesome component
app.component('FaIcon', FontAwesomeIcon);

// Use the router we configured
app.use(router);

// Use OpenLayers
app.use(OpenLayersMap);

// ---------------------------------------------------------------------------------------------------------------------

// Initialization
async function init() : Promise<void>
{
    // Get current user, if any
    await authMan.loadCurrentUser();

    // Load ships
    await shipMan.init();

    // Load layers
    await layerMan.init();

    // Print out when init is done
    console.info('RFI Map initialized.');
}

// ---------------------------------------------------------------------------------------------------------------------

init()
    .then(() => app.mount('#rfi-map'))
    .catch((err) =>
    {
        console.error('Failed to initialize application:', err.stack);
    });

// ---------------------------------------------------------------------------------------------------------------------
