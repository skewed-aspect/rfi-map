// ---------------------------------------------------------------------------------------------------------------------
// Configuration Interfaces
// ---------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';

// ---------------------------------------------------------------------------------------------------------------------

export interface HttpConfig
{
    host : string;
    port : number;
    secure : boolean;
}

export interface GoogleAuthConfig
{
    clientID : string;
    clientSecret : string;
}

export interface AuthConfig
{
    session : {
        key : string;
        secret : string;
    },
    google : GoogleAuthConfig;
    users ?: {
        admins : string[];
    }
}

export interface DatabaseConfig extends Knex.Config
{
    traceQueries : boolean;
}

export interface ServerConfig
{
    http : HttpConfig;
    auth : AuthConfig;
    database : DatabaseConfig;
}

// ---------------------------------------------------------------------------------------------------------------------
