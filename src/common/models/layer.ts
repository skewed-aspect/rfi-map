//----------------------------------------------------------------------------------------------------------------------
// Layer Interface
//----------------------------------------------------------------------------------------------------------------------

import { Feature, GeoJsonProperties, LineString, Point, Polygon } from 'geojson';

import { FlatStyleLike } from 'ol/style/flat.js';

//----------------------------------------------------------------------------------------------------------------------

export const validLayerTypes = [ 'point', 'poly' ] as const;
export type LayerType = typeof validLayerTypes[number];

export type LayerGeometry = Point | LineString | Polygon;
export type LayerProperties = GeoJsonProperties & { style ?: FlatStyleLike };
export type LayerFeature = Feature<LayerGeometry, LayerProperties>;

export interface Layer
{
    name : string;
    type : LayerType;
    description : string;
    features : LayerFeature[];
    style : FlatStyleLike;
}

//----------------------------------------------------------------------------------------------------------------------
